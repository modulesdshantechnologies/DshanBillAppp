/**
 * Created by zendynamix on 25-11-2016.
 */
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    clientsDetailsModel = mongoose.model('clientDetails');
    clientsFromConfig=require('../fromConfig/clients.json')

    module.exports = function (app){
        app.use('/', router);
    };
router.get('/clientJsonConfig', function (req, res) {
    res.send(clientsFromConfig);
});

router.post('/clientDetails', function(req, res, next) {
    console.log("****")
    console.log(req.body)
    var newclientDetails = new clientsDetailsModel(req.body);
    newclientDetails.save(function(err) {
        if (err){
            console.log('Error in Saving user: '+err);
        }

        res.send("client added sucessfully");
    });

})


router.get('/clientDetails/count', function (req, res){
    clientsDetailsModel.count(function(err,clientCount){
        if(err)
            res.send(err);
        var count = {clientCount: clientCount};
        res.send(count);
    });
})

router.get('/clientDetails/:start/:range', function (req, res) {
    console.log("server side")
    clientsDetailsModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
})

router.delete('/clientDetails/:id', function (req, res){
    clientsDetailsModel.remove({_id:req.params.id},function (err) {
        if(err)
            res.send(err)
        res.send(' companyDetails Deleted')
    });
})

router.get('/clientDetails', function (req, res) {
    clientsDetailsModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })
})

router.post('/clientDetails/update', function (req, res) {
    clientsDetailsModel.findOneAndUpdate(
        { "_id" : req.body.mondbId},
        req.body, // document to insert
        {upsert: true, new: true}, // options
        function (err, updatedBike) { // callback
            if (err) console.log('ERROR '+ err);
            else res.json("updated successfully")

        });

})


router.get('/clientDetails/:clientName', function (req, res) {
    console.log(req.params.clientName)
    clientsDetailsModel.find({clientName:req.params.clientName},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })
})

router.get('/clientDetailsById/:clientId', function (req, res) {
    console.log(req.params.clientId)
    clientsDetailsModel.find({_id:req.params.clientId},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })
})

router.get('/clientDetailsName', function (req, res) {
    clientsDetailsModel.find({},{_id:0},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })
})

router.get('/allclientDetails', function (req, res) {
    clientsDetailsModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })
})
