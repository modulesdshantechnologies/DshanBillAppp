/**
 * Created by Suhas on 3/8/2016.
 */
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    companyDetailsModel = mongoose.model('companyDetails');
    companyFromConfig=require('../fromConfig/company.json')
        schemaUtility=require('../utility').fromSchema
    module.exports = function (app){
        app.use('/', router);
    };


router.get('/CompanyJsonConfig', function (req, res) {
   res.send(companyFromConfig);
});



router.post('/companyDetails', function(req, res, next) {
    console.log("**********req.body************")
    console.log(req.body)
    console.log("**********req.body************")
    companyDetailsModel.remove({}, function(err,res) {
        if(err){
            console.log("err occurred")
        }
        var newCompanyDetails = new companyDetailsModel();
        newCompanyDetails.companyName=req.body.companyName;
        newCompanyDetails.country=req.body.country;
        newCompanyDetails.city=req.body.city;
        newCompanyDetails.pinCode=req.body.pinCode;
        newCompanyDetails.website=req.body.website,
        newCompanyDetails.taxationType=req.body.taxationType,
        newCompanyDetails.contactName=req.body.contactName,
        newCompanyDetails.deafultCurrency=req.body.deafultCurrency,
        newCompanyDetails.state=req.body.state,
        newCompanyDetails.address=req.body. address,
        newCompanyDetails.email=req.body.email,
        newCompanyDetails.phone=req.body.phone,
        newCompanyDetails.serviceTaxNo=req.body.serviceTaxNo,
        newCompanyDetails.tin=req.body.tin,
        newCompanyDetails.vat=req.body.vat,
        newCompanyDetails.cst=req.body.cst,
        newCompanyDetails.pan=req.body.pan,
        newCompanyDetails.lst=req.body.lst
        newCompanyDetails.save(function(err) {
            if (err){
                console.log('Error in Saving user: '+err);
            }

        });
    });



    res.send("company Details added sucessfully");

})

router.get('/companyDetails/count', function (req, res){
    companyDetailsModel.count(function(err,companyCount){
        if(err)
            res.send(err);
        var count = {companyCount: companyCount};
        res.send(count);
    });
})

router.get('/companyDetails/:start/:range', function (req, res) {
    console.log("server side")
    companyDetailsModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
})



router.delete('/companyDetails/:id', function (req, res){
    companyDetailsModel.remove({_id:req.params.id},function (err) {
        if(err)
            res.send(err)
        res.send(' companyDetails Deleted')
    });
})

router.get('/companyDetails', function (req, res) {
    companyDetailsModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)

        }

    })
})
router.get('/companylastDetails', function (req, res) {
     companyDetailsModel.findOne({}).sort({"companyName":1}).skip(counts).limit().exec(function(err,result){
           if(err){
           res.send(err)
           console.log(err.stack)
           }else{
           res.send(result)

            }
       })
companyDetailsModel.count(function(err,companyCount){
        if(err)
            res.send(err);
        var count = {companyCount: companyCount};
        res.send(count);
    });

})

router.post('/companyDetails/update', function (req, res) {
    console.log("****************************")
    console.log(req.body)
    companyDetailsModel.findOne({'_id':req.body._id}, function (err, companyDetails) {
        if (err)
            res.send(err);
        if(companyDetails){
            companyDetails.companyName=req.body.companyName;
            companyDetails.streetAddress=req.body.streetAddress;
            companyDetails.city=req.body.city;
            companyDetails.Street=req.body.Street;
            companyDetails.ZipCode=req.body.ZipCode;
            companyDetails.Phone=req.body.Phone;
            companyDetails.Fax=req.body.Fax;
            companyDetails.webSite=req.body.webSite;
            companyDetails.Logo=req.body.Logo;
            companyDetails.tin=req.body.tin;
            companyDetails.vat=req.body.vat;
            companyDetails.cst=req.body.cst;
            companyDetails.pan=req.body.pan;
            companyDetails.lst=req.body.lst;

            companyDetails.save(function(err){
                if(err)
                    res.send(err)

                res.send(' successfully updated')
            })
        }

    });
})



router.post('/updateCompanyLogoDetails', function (req, res) {
    console.log("updateCompanyLogoDetailsupdateCompanyLogoDetails#############")
    console.log(req.body)
    companyDetailsModel.findOne({'_id':req.body.companyDataMongoId}, function (err, companyDetails) {
        if (err)
            res.send(err);
        if(companyDetails){
            companyDetails.Logo=req.body.Logo;
            companyDetails.update(function(err){
                if(err)
                    res.send(err)
                res.send(' successfully updated')
            })
        }

    });
})

