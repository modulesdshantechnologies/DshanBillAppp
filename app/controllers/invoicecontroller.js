/**
 * Created by dhanalakshmi on 15-01-2017.
 */
//var express = require('express'),
//    router = express.Router(),
//    mongoose = require('mongoose'),
//   invoiceModel = mongoose.model('invoice');
//
//    module.exports = function (app){
//        app.use('/', router);
//    };
//
//router.post('/invoiceDetails', function(req, res, next) {
//    console.log("****")
//    console.log(req.body)
//    var newInvoiceDetails = new invoiceModel(req.body);
//    newInvoiceDetails.save(function(err) {
//        if (err){
//            console.log('Error in Saving user: '+err);
//        }
//        res.send("newInvoiceDetails added sucessfully");
//    });
//
//})



/*

{

    Duedate:
        Invoicedate:
            ClientName
    PoNo
    Paymentterms
    productOrderDetails:{
        productName
        Quantity
        productUnit
        unitPrice
        Discount
        tax
    }
    shippingDetails:{
        CompanyName:
            CompanyShippingAddress
        ShippingStreetAddress
        ShippingCity
        ShippingState
    }

}

Payment Methods*/
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    InvoicesDetailsModel = mongoose.model('invoice');
    InvoicesFromConfig=require('../fromConfig/Invoice.json')

    module.exports = function (app){
        app.use('/', router);
    };
router.get('/InvoiceJsonConfig', function (req, res) {
    res.send(InvoicesFromConfig);
});

router.post('/InvoiceDetails', function(req, res, next) {
    console.log("****")
    console.log(req.body)
    var newInvoiceDetails = new InvoicesDetailsModel(req.body);
    newInvoiceDetails.save(function(err) {
        if (err){
            console.log('Error in Saving user: '+err);
        }

        res.send("Invoice added sucessfully");
    });

})


router.get('/InvoiceDetails/count', function (req, res){
    InvoicesDetailsModel.count(function(err,InvoiceCount){
        if(err)
            res.send(err);
        var count = {InvoiceCount: InvoiceCount};
        res.send(count);
    });
})

router.get('/InvoiceDetails/:start/:range', function (req, res) {
    console.log("server side")
    InvoicesDetailsModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
})

router.delete('/InvoiceDetails/:id', function (req, res){
    InvoicesDetailsModel.remove({_id:req.params.id},function (err) {
        if(err)
            res.send(err)
        res.send(' companyDetails Deleted')
    });
})

router.get('/InvoiceDetails', function (req, res) {
    InvoicesDetailsModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })
})

router.post('/InvoiceDetails/update', function (req, res) {
    InvoicesDetailsModel.findOneAndUpdate(
        { "_id" : req.body.mondbId},
        req.body, // document to insert
        {upsert: true, new: true}, // options
        function (err, updatedBike) { // callback
            if (err) console.log('ERROR '+ err);
            else res.json("updated successfully")

        });

})


router.get('/InvoiceDetails/:InvoiceName', function (req, res) {
    console.log(req.params.InvoiceName)
    InvoicesDetailsModel.find({InvoiceName:req.params.InvoiceName},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })
})

router.get('/InvoiceDetailsById/:InvoiceId', function (req, res) {
    console.log(req.params.InvoiceId)
    InvoicesDetailsModel.find({_id:req.params.InvoiceId},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })
})

router.get('/InvoiceDetailsName', function (req, res) {
    InvoicesDetailsModel.find({},{"_id":0,"InvoiceName":1},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })
})

router.get('/allInvoiceDetails', function (req, res) {
    InvoicesDetailsModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })
})






