/**
 * Created by zendynamix on 07-03-2016.
 */
var mongoose = require('mongoose'),
    config = require('../../config/config');
var PassportLocal = require('../models/userDetails');
Schema = mongoose.Schema;
mongoose.connect(config.db);
PassportLocal.findOneAndRemove({"username": 'admin'}, function(err){
    if(err) {
        console.log(err)
    }else{
            console.log("removed previous admin document   ")
            generateUserCredencials();
        }

});

function generateUserCredencials(){
    var userObj = new PassportLocal();
    userObj.username="admin";
    userObj.password="$2a$10$7fu8phzzG3o7XdaVb6xZX.Ac4xuBkDb3zPFBNmxmgW96QUeWDLv2C";
    userObj.email="admin@gmail.com";
    userObj.firstName="admin";
    userObj.lastName="smrt";
    userObj.isAdmin=true;
    userObj.save(function(err,result){
        if(err)
            console.log(err)
        console.log("Credencials generated")
        process.exit(0);
    })
};


