/**
 * Created by zendynamix on 25-11-2016.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    clientFromConfig=require('../fromConfig/clients.json');
schemaObject=require('../utility').fromSchema.fromSchemaBasedOnJson(clientFromConfig);

var clientsDetailsSchema = new mongoose.Schema(
    schemaObject,{collection: "clientDetails"});

module.exports =mongoose.model('clientDetails',clientsDetailsSchema);