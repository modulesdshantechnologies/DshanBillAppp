/**
 * Created by zendynamix on 20-11-2016.
 */
/*var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    companyFromConfig=require('../fromConfig/company.json')
    schemaObject=require('../utility').fromSchema.fromSchemaBasedOnJson(companyFromConfig);
var companySchema = new mongoose.Schema(
    schemaObject,{collection: "companyDetails"});

module.exports =mongoose.model('companyDetails',companySchema);*/


/*var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var companySchema = new mongoose.Schema({companyDetails:{}},{collection:"companyDetails"});

module.exports = mongoose.model('companyDetails',companySchema)*/

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var companySchema = new mongoose.Schema(
    {
        companyName:String,
        country:String,
        city:String,
        pinCode:Number,
        website:String,
        taxationType:String,
        contactName:String,
        deafultCurrency:String,
        state:String,
        address:String,
        email:String,
        phone:Number,
        serviceTaxNo:Number,
        tin:Number,
        vat:Number,
        cst:Number,
        pan:Number,
        lst:Number


    },{collection:'companyDetails'});
mongoose.model('companyDetails', companySchema);