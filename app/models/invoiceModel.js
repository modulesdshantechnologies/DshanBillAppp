/**
 * Created by dhanalakshmi on 15-01-2017.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
InvoiceFromConfig=require('../fromConfig/Invoice.json');
schemaObject=require('../utility').fromSchema.fromSchemaBasedOnJson(InvoiceFromConfig);
var invoiceSchema = new mongoose.Schema(
    {
        invoiceNumber:Number,
        InvoiceRaisedDate:Date,
        InvoiceDueDate:Date,
        clientName:String,
        InvoicePono:Number,
        InvoicePaymentterms:Number,
        productOrderDetails:[{
            productName:String,
            Quantity:Number,
            productUnit:Number,
            unitPrice:Number,
            discount:Number,
            tax:Number
        }],
        shippingDetails:{
            CompanyName:String,
            CompanyShippingAddress:String,
            ShippingStreetAddress:String,
            ShippingCity:String,
            ShippingState:String,
            ShippingZipCode:String,
            ShippingPhoneNumber:String,
            ShippingCharges:Number
        },
        invoiceDesp:String,
        subTotal:Number,
        serviceTax:Number,
        discount:Number,
        TotalInvoiceCost:Number,
        DueAmount:Number,
        TermsandConditions:String,
        PrivateNotes:String
 },{collection:'invoice'});
 module.exports=mongoose.model('invoice', invoiceSchema);