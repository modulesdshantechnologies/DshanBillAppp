var billApp = angular.module('billApp', ['ui.router','angularFromUI','angularEditFromUI','angularFromInvoiceUI','angularUtils.directives.dirPagination', 'ui.bootstrap','datepicker','angular.chosen']);


//billApp.run(['$route', '$rootScope', '$location', function ($route, $rootScope, $location) {
//    var original = $location.path;
//    $location.path = function (path, reload) {
//        if (reload === false) {
//            var lastRoute = $route.current;
//            var un = $rootScope.$on('$locationChangeSuccess', function () {
//                $route.current = lastRoute;
//                un();
//            });
//        }
//        return original.apply($location, [path]);
//    };
//}])
//



billApp.run(function(clientService,productService,companyService,invoiceService){
//function controllerpreventing()
//{
// var original = $location.path;
//    $location.path = function (path, reload) {
//        if (reload === false) {
//            var lastRoute = $route.current;
//            var un = $rootScope.$on('$locationChangeSuccess', function () {
//                $route.current = lastRoute;
//                un();
//            });
//        }
//        return original.apply($location, [path]);
//        }
//    };
//



    function getClientJsonConfig(){
        clientService.getclientJsonConfig().then(function (resultDetails) {
            clientService.setclientFromConfig(resultDetails.data)
        }, function error(errResponse) {
            console.log("cannot get settings config")
        })

    }
    function getProductJsonConfig(){
        productService.getProductJsonConfig().then(function (resultDetails) {
            productService.setProductFromConfig(resultDetails.data)
            console.log(productService.getProductFromConfig())
        }, function error(errResponse) {
            console.log("cannot get settings config")
        })
        }
     function getInvoiceJsonConfig(){
             invoiceService.getInvoiceJsonConfig().then(function (resultDetails) {
                invoiceService.setInvoiceFromConfig(resultDetails.data)
                 console.log(invoiceService.getInvoiceFromConfig())
             }, function error(errResponse) {
                 console.log("cannot get settings config")
             })

    }
    function getCompanyJsonConfig(){
        companyService.getCompanyJsonConfig().then(function (resultDetails) {
        console.log(resultDetails.data)
            companyService.setCompanyFromConfig(resultDetails.data);
        }, function error(errResponse) {
            console.log("cannot get settings config")
        })
    }

function getCompanydetails(){
        companyService.getCompanyDetails().then(function (resultDetails) {
        console.log(resultDetails.data)
        }, function error(errResponse) {
            console.log("cannot get settings config")
        })
    }


    function init(){

        getClientJsonConfig();
        getProductJsonConfig();
        getInvoiceJsonConfig();
        getCompanyJsonConfig();
        getCompanydetails();


    }
    init();

});

billApp.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('login');

    $stateProvider

        // HOME STATES AND NESTED VIEWS ========================================

        .state('login', {
         url: "/login",
         templateUrl:'template/login.html',
         controller:'loginCtrl'
        })

        .state('mainpage', {
        url: "/mainpage",
        templateUrl:'template/mainpage.html',
        abstract:true

        })

        .state('mainpage.dashboard', {
        url: "/dashboard",
        templateUrl:'template/dashboard.html',
        controller:'dashboardCtrl'

        })
      
       .state('mainpage.clients', {
        url: '/clients',
        templateUrl:'template/client.html',
        controller:'clientCtrl'
         })

        .state('mainpage.invoice', {
        url: '/invoice',
        templateUrl:'template/invoice.html',
//        controller:'clientCtrl'
          controller:'invoiceCtrl'

         })

        .state('mainpage.invoicegen', {
         url: '/invoicegen',
         templateUrl:'template/invoicegeneration.html',
         controller:'invoiceCtrl'

          })

           .state('mainpage.invoicelist', {
             url: '/invoicelist',
             templateUrl:'template/invoiceDetails.html',
              controller:'invoiceCtrl'
                    })

        .state('mainpage.products', {
         url: '/products',
         templateUrl:'template/products.html',
         controller:'productCtrl'
         })

        .state('mainpage.company', {
         url: '/companySettings',
          templateUrl:'template/companySettings.html',
          controller:'companyController'
          })

});




billApp.directive('exportTable', function(){
    var link = function ($scope, elm, attr) {
        console.log("*************************")
        console.log(elm)
        $scope.$on('export-pdf', function (e, d) {
            elm.exportDetails({ type: 'pdf', escape: false });
        });
        $scope.$on('export-excel', function (e, d) {
            elm.exportDetails({ type: 'excel', escape: false });
        });
        $scope.$on('export-doc', function (e, d) {
            elm.exportDetails({ type: 'doc', escape: false });
        });
        $scope.$on('export-csv', function (e, d) {
            elm.exportDetails({ type: 'csv', escape: false });
        });
    }
    return {
        restrict: 'A',
        link: link
    }
});






billApp.directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function(){
                    scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }]);

