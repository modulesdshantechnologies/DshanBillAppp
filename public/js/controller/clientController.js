/**
 * Created by zendynamix on 25-11-2016.
 */
billApp.controller("clientCtrl", function ($scope, clientService, fromService) {

    $scope.exportAction = function (option) {
        switch (option) {
            case 'pdf':
                $scope.$broadcast('export-pdf', {});
                break;
            case 'excel':
                $scope.$broadcast('export-excel', {});
                break;
            case 'doc':
                $scope.$broadcast('export-doc', {});
                break;
            case 'csv':
                $scope.$broadcast('export-csv', {});
                break;
            default:
                console.log('no event caught');
        }
    };

    $scope.formvisible = false;
    $scope.tablevisible = true;
    $scope.showhide = function () {
        $scope.formvisible = $scope.formvisible ? false : true;
        $scope.tablevisible = $scope.tablevisible ? false : true;

    };
    $scope.form2visible = false;
    $scope.showhide2 = function () {
        $scope.form2visible = $scope.form2visible ? false : true;
        $scope.tablevisible = $scope.tablevisible ? false : true;
    };

    $scope.clientDetails = {
        detailsArray: [],
        updateMessage: "",
        deleteMessage: "",
        mongodbIdForDelete: "",
        mongodbId: ""
    }

    $scope.getclientDetailsByRange = function (pageNo) {
        var pageCapacity = 10;
        var start = 0;
        clientService.getclientDetailsByRange(start, pageCapacity).then(function (res) {
            $scope.clientDetails.detailsArray = res.data.reverse();
        })
    }

    $scope.editclientDetails = function (clientDetails) {
        clientService.updateclientDetails(clientDetails).then(function (res) {
            $scope.clientDetails.updateMessage = res.data;
        })
    }
    $scope.deleteclientDetails = function (clientMongoDbId) {
        clientService.deleteclientDetails(clientMongoDbId).then(function (res) {
            $scope.clientDetails.deleteMessage = res.data;
            $scope.getclientDetailsByRange(0)
        })
    }
    $scope.getConfigForclientSaveFrom = function () {
        var modifiedSaveConfig = fromService.convertJsonToArray(clientService.getclientFromConfig())
        $scope.clientJsonConfig = modifiedSaveConfig

    }
    $scope.saveclientDetails = function (clientDetails) {
        $scope.unquieclientNameError = ""
        for (var k = 0; k < clientDetails.length; k++) {
            if (clientDetails[k].realName === "CompanyName") {
                clientService.getclientDetailsByName(clientDetails[k].modelValue).then(function (res) {
                    if (res.data[0]) {
                        $scope.unquieclientNameError = "client name already exists"
                    }
                    else {
                        var saveObj = {}
                        var clientSaveObj = {}
                        for (var k = 0; k < clientDetails.length; k++) {
                            saveObj = clientDetails[k]
                            clientSaveObj[saveObj.realName] = saveObj.modelValue
                            if (k === clientDetails.length - 1) {
                                clientService.saveclientDetails(clientSaveObj).then(function (resultDetails) {
                                    $scope.getclientDetailsByRange(0)

                                }, function error(errResponse) {
                                    console.log(errResponse)
                                })
                            }


                        }


                    }

                })
            }

        }
        jQuery('#btnClose').click();
        jQuery('#success').click()

    }

    $scope.getclientDetailsById = function (clientDetails) {
        $scope.clientDetails.mongodbId = clientDetails._id;
        MergeEditFrom(clientDetails, clientService.getclientFromConfig())
    }

    function MergeEditFrom(clientDetails, clientFromConfig) {
        var obj = {}
        var editObj = {}
        var k = Object.keys(clientFromConfig);
        k.forEach(function (objkey, index) {
            var obj = {}
            obj.description = clientFromConfig[k[index]].description
            obj.modelValue = clientDetails[objkey]
            obj.type = clientFromConfig[k[index]].type
            editObj[objkey] = obj

        });
        var clientEditObj = fromService.convertJsonToArray(editObj)


        $scope.clientEditJsonConfig = clientEditObj;


    }

    $scope.updateclientDetailsToDb = function (clientDetails) {
        var editObj = {}
        var clientEditObj = {}
        for (var k = 0; k < clientDetails.length; k++) {
            editObj = clientDetails[k]
            clientEditObj[editObj.realName] = editObj.modelValue
            clientEditObj.mondbId = $scope.clientDetails.mongodbId
            if (k === clientDetails.length - 1) {

                clientService.updateclientDetails(clientEditObj).then(function (res) {

                    $scope.getclientDetailsByRange(0)
                })
            }


        }
        jQuery('#btnsClose').click();
        jQuery('#info').click()

    }
    $scope.setclientDetailsIdForDelete = function (mongodbId) {
        $scope.clientDetails.mongodbIdForDelete = mongodbId;

    }
    $scope.deleteClientDetails = function (clientMongoDbId) {
        clientService.deleteclientDetails(clientMongoDbId).then(function (res) {
            $scope.clientDetails.deleteMessage = res.data;
            $scope.getclientDetailsByRange(0)
            jQuery('#deletebtnClose').click();
            jQuery('#warning').click()

        })
    }

    function init() {
        $scope.getclientDetailsByRange(0)
        $scope.getConfigForclientSaveFrom();
    }

    init();
})