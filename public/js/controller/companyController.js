/**
 * Created by zendynamix on 20-11-2016.
 */

billApp.controller("companyController", function ($scope, $state, companyService, fileUpload) {

    $scope.showmodal = function () {
        angular.element('#myModal').modal('show');
    }


    $scope.settingsCtrl = {
        detailsArray: [],
        deleteMessage: "",
        updateMessage: ""
    }
    $scope.formCompanyDetails = {}
    $scope.companyJsonConfig = companyService.getCompanyFromConfig();

    $scope.saveCompanyDetails = function (CompanyDetails) {
        companyService.saveCompanyDetails(CompanyDetails).then(function (resultDetails) {
            console.log(resultDetails)
            jQuery('#success').click()

        }, function error(errResponse) {
            console.log(errResponse)
        })
    }

    $scope.getcompanydetails = function () {

        companyService.getCompanyLastDetails().then(function (resultDatails) {

            console.log(resultDatails.data)
        }, function error(errresponse) {
            console.log(errresponse)
        })

    }

    $scope.editCompanyDetails = function (companyDetails) {
        console.log("***")
        console.log(companyDetails)
        companyService.updateCompanyDetails(companyDetails).then(function (res) {
            $scope.settingsCtrl.updateMessage = res.data;
        })
    }
    $scope.deleteCompanyDetails = function (companyMongoDbId) {
        companyService.deleteCompanyDetails(companyMongoDbId).then(function (res) {
            $scope.settingsCtrl.deleteMessage = res.data;
            $scope.getCompanyDetailsByRange(0)
        })
    }

    $scope.getSettingsCompanyDetails = function () {
        companyService.getCompanyDetails().then(function (res) {
            console.log(res.data);
            $scope.formCompanyDetails = res.data[0];

        })
    }

    $scope.uploadFile = function () {

        var file = $scope.myFile;

        console.log('file is ');
        console.dir(file);
        fileUpload.uploadFileToUrl(file).then(function (response) {

            $scope.showmodal();
            jQuery('#info').click()


            companyService.updateCompanyLogoDetails($scope.formCompanyDetails._id, response.data).then(function (res) {

                $scope.settingsCtrl.updateMessage = res.data;
            })
        })
    };


    function init() {
        $scope.getSettingsCompanyDetails();
        $scope.getcompanydetails();

    }

    init()


})

