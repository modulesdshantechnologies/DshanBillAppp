/**
 * Created by Suhas on 12/5/2015.
 */

billApp.controller('loginCtrl', function ($scope, $http, $window, $location, $state, $rootScope, authInterceptor) {
    $scope.checkValidationdddd = "opopopo"
    $scope.checkValidation = function (user) {
        console.log("********************useruser")
        console.log(user)
        if (user && user.username && user.password) {
            $state.go('mainpage.dashboard')

        } else if (!user) {
            $scope.message = "Username or password required";
        }
    };


    $scope.logout = function () {
        delete $window.sessionStorage.token;
        $rootScope.admin = false;
        $state.go('admin')
    };


    $scope.signUp = function (user) {
        $http
            .post('/signup', user)
            .success(function (data, status, headers, config) {
                $scope.successMessage = "Successfully registered Please login";
                $state.go('admin')
            })
            .error(function (data, status, headers, config) {
                $scope.error = data;
            });
    };

    $scope.setFlag = function () {
        $rootScope.admin = false;
    };

    $scope.enterToLogin = function (event) {
        if (event.keyCode == 13) {
            $scope.checkValidation($scope.user);
        }
    }
});

