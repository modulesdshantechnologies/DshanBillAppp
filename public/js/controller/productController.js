///**
// * Created by zendynamix on 25-11-2016.
// */
billApp.controller("productCtrl", function ($scope, productService, fromService) {
//$scope.click = function () {
//    AlertsServ.logMessage("Title", "Some message")
//  }

    /*
     toastr.success('Hello world!', 'Toastr fun!');
     */

    $scope.sortcolumn = "name";
    $scope.reversesort = false;
    $scope.sortdata = function (column) {
        $scope.reversesort = ($scope.sortcolumn == column) ? !$scope.reversesort : false;
        $scope.sortcolumn = column;
    };
    $scope.getsortclass = function (column) {
        if ($scope.sortcolumn == column) {
            return $scope.reversesort ? 'arrow-down' : 'arrow-up'
        }
        return '';
    };
    $scope.productDetails = {
        detailsArray: [],
        updateMessage: "",
        deleteMessage: "",
        mongodbIdForDelete: "",
        mongodbId: ""
    }

    $scope.getProductDetailsByRange = function (pageNo) {
        var pageCapacity = 10;
        var start = 0;
        productService.getProductDetailsByRange(start, pageCapacity).then(function (res) {
            $scope.productDetails.detailsArray = res.data.reverse();
        })
    }

    $scope.editProductDetails = function (productDetails) {
        console.log("***")
        console.log(productDetails)
        productService.updateProductDetails(productDetails).then(function (res) {
            $scope.productDetails.updateMessage = res.data;
        })
    }
    $scope.deleteProductDetails = function (ProductMongoDbId) {
        productService.deleteProductDetails(ProductMongoDbId).then(function (res) {
            $scope.productDetails.deleteMessage = res.data;
            $scope.getProductDetailsByRange(0)
        })
    }
    $scope.getConfigForProductSaveFrom = function () {
        var modifiedSaveConfig = fromService.convertJsonToArray(productService.getProductFromConfig())
        $scope.productJsonConfig = modifiedSaveConfig

    }
    $scope.saveProductDetails = function (productDetails) {
        console.log(productDetails)
        $scope.unquieProductNameError = ""
        for (var k = 0; k < productDetails.length; k++) {
            if (productDetails[k].realName === "ProductName") {
                productService.getProductDetailsByName(productDetails[k].modelValue).then(function (res) {
                    console.log(res.data[0])
                    if (res.data[0]) {
                        $scope.unquieProductNameError = "Product name already exists"
                    }
                    else {
                        var saveObj = {}
                        var productSaveObj = {}
                        for (var k = 0; k < productDetails.length; k++) {
                            saveObj = productDetails[k]
                            productSaveObj[saveObj.realName] = saveObj.modelValue
                            if (k === productDetails.length - 1) {
                                productService.saveProductDetails(productSaveObj).then(function (resultDetails) {
                                    $scope.getProductDetailsByRange(0)
                                    console.log(resultDetails)
                                    jQuery('#success').click()
                                }, function error(errResponse) {
                                    console.log(errResponse)
                                })
                            }
                        }
                    }
                })
            }
        }
        jQuery('#savebtncancel').click();

    }

    $scope.getProductDetailsById = function (productDetails) {
        $scope.productDetails.mongodbId = productDetails._id;
        MergeEditFrom(productDetails, productService.getProductFromConfig())
    }

    function MergeEditFrom(productDetails, productFromConfig) {
        var obj = {}
        var editObj = {}
        var k = Object.keys(productFromConfig);
        k.forEach(function (objkey, index) {
            var obj = {}
            obj.description = productFromConfig[k[index]].description
            obj.modelValue = productDetails[objkey]
            obj.type = productFromConfig[k[index]].type
            editObj[objkey] = obj

        });
        var productEditObj = fromService.convertJsonToArray(editObj)


        $scope.productEditJsonConfig = productEditObj;


    }

    $scope.updateProductDetailsToDb = function (productDetails) {
        var editObj = {}
        var productEditObj = {}
        for (var k = 0; k < productDetails.length; k++) {
            editObj = productDetails[k]
            productEditObj[editObj.realName] = editObj.modelValue
            productEditObj.mondbId = $scope.productDetails.mongodbId
            if (k === productDetails.length - 1) {

                productService.updateProductDetails(productEditObj).then(function (res) {

                    $scope.getProductDetailsByRange(0)
                })
            }


        }
        jQuery('#editbtncancel').click();
        jQuery('#info').click()


    }
    $scope.setProductDetailsIdForDelete = function (mongodbId) {
        $scope.productDetails.mongodbIdForDelete = mongodbId;

    }
    $scope.deleteClientDetails = function (productMongoDbId) {
        productService.deleteProductDetails(productMongoDbId).then(function (res) {
            $scope.productDetails.deleteMessage = res.data;
            $scope.getProductDetailsByRange(0)
            jQuery('#deletebtncancel').click();
            jQuery('#warning').click()

        })
    }

    $scope.exportAction = function (option) {
        console.log("23874298379827349852739457" + option)
        switch (option) {
            case 'pdf':
                $scope.$broadcast('export-pdf', {});
                break;
            case 'excel':
                $scope.$broadcast('export-excel', {});
                break;
            case 'doc':
                $scope.$broadcast('export-doc', {});
                break;
            case 'csv':
                $scope.$broadcast('export-csv', {});
                break;
            default:
                console.log('no event caught');
        }
    };


    function init() {
        $scope.getProductDetailsByRange(0)
        $scope.getConfigForProductSaveFrom();
    }

    init();


    /*ngToast.create('a toast message...');

     // create a toast with settings:
     ngToast.create({
     className: 'warning',
     content: '<a href="#" class="">a message</a>'
     });

     // or just use "success", "info", "warning" or "danger" shortcut methods:
     var myToastMsg = ngToast.warning({
     content: '<a href="#" class="">a message</a>'
     });

     // to clear a toast:
     ngToast.dismiss(myToastMsg);

     // clear all toasts:
     ngToast.dismiss();*/

//    $(function () {
//        $('#error').click(function () {
//            // make it not dissappear
//            toastr.error("Noooo oo oo ooooo!!!", "Title", {
//                "timeOut": "0",
//                "extendedTImeout": "0"
//            });
//        });
//        $('#info').click(function () {
//       		// title is optional
//            toastr.info("Info Message", "Title");
//        });
//        $('#warning').click(function () {
//            toastr.warning("Warning");
//        });
//        $('#success').click(function () {
//            toastr.success("YYEESSSSSSS");
//        });
//    });
//
})