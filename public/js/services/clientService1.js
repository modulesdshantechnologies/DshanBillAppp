/**
 * Created by zendynamix on 25-11-2016.
 */
/**
 * Created by zendynamix on 25-11-2016.
 */
billApp.factory("clientService", function ($http) {
    var formConfigObj = {};
    var clientCount;
    var getclientJsonConfig = function () {
        return $http.get('/clientJsonConfig');
    }
    var setclientFromConfig = function (formConfig) {
        formConfigObj = formConfig
    }
    var getclientFromConfig = function () {
        return formConfigObj
    }
    var saveclientDetails = function (clientData) {
        return $http.post('/clientDetails', clientData);
    }
    var getclientDetailsByRange = function (start, range) {
        return $http.get('/clientDetails/' + start + '/' + range);
    }
    var getclientDetailsCount = function () {
        return $http.get('/clientDetails/count');
    }
    var getclientCount = function () {
        return clientCount;
    }
    var setclientCount = function (val) {
        clientCount = val;
    }
    var updateclientDetails = function (clientDetails) {
        return $http.post('/clientDetails/update', clientDetails)
    }
    var deleteclientDetails = function (id) {
        return $http.delete('/clientDetails/' + id);
    }

    var getclientDetailsByName = function (clientName) {
        return $http.get('/clientDetails/' + clientName);
    }

    var getclientDetailsById = function (clientId) {
        return $http.get('/clientDetailsById/' + clientId);
    }

    var getAllclientName = function () {
        return $http.get('/clientDetailsName');
    }
    var getAllclient = function () {
        return $http.get('/allclientDetails');
    }


    return {
        getclientJsonConfig: getclientJsonConfig,
        setclientFromConfig: setclientFromConfig,
        getclientFromConfig: getclientFromConfig,
        saveclientDetails: saveclientDetails,
        getclientDetailsByRange: getclientDetailsByRange,
        getclientDetailsCount: getclientDetailsCount,
        getclientCount: getclientCount,
        setclientCount: setclientCount,
        updateclientDetails: updateclientDetails,
        deleteclientDetails: deleteclientDetails,
        getclientDetailsByName: getclientDetailsByName,
        getclientDetailsById: getclientDetailsById,
        getAllclientName: getAllclientName,
        getAllclient: getAllclient
    }
})