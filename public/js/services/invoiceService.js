/**
 * Created by dhanalakshmi on 4/3/17.
 */

billApp.factory("invoiceService", function ($http) {
//
//    var saveInvoiceDetails = function(invoiceData){
//        return $http.post('/invoiceDetails',invoiceData);
//    }
//
//    return{
//        saveInvoiceDetails:saveInvoiceDetails
//    }

var invoicedate;
var duedate;
    var formConfigObj = {};
    var invoicePreviewconfig = {};
    var InvoiceCount;
    var invoicefullconfig={};
    var getInvoiceJsonConfig = function () {
        return $http.get('/InvoiceJsonConfig');
    }
    var setInvoiceFromConfig = function (formConfig) {
        formConfigObj = formConfig
    }
    var getInvoiceFromConfig = function () {
        return formConfigObj
    }
    var saveInvoiceDetails = function (InvoiceData) {
        return $http.post('/InvoiceDetails', InvoiceData);
    }
    var getInvoiceDetailsByRange = function (start, range) {
        return $http.get('/InvoiceDetails/' + start + '/' + range);
    }
    var getInvoiceDetailsCount = function () {
        return $http.get('/InvoiceDetails/count');
    }
    var getInvoiceCount = function () {
        return InvoiceCount;
    }
    var setInvoiceCount = function (val) {
        InvoiceCount = val;
    }
    var updateInvoiceDetails = function (InvoiceDetails) {
        return $http.post('/InvoiceDetails/update', InvoiceDetails)
    }
    var deleteInvoiceDetails = function (id) {
        return $http.delete('/InvoiceDetails/' + id);
    }

    var getInvoiceDetailsByName = function (InvoiceName) {
        return $http.get('/InvoiceDetails/' + InvoiceName);
    }

    var getInvoiceDetailsById = function (InvoiceId) {
        return $http.get('/InvoiceDetailsById/' + InvoiceId);
    }

    var getAllInvoiceName = function () {
        return $http.get('/InvoiceDetailsName');
    }
    var getAllInvoice = function () {
        return $http.get('/allInvoiceDetails');
    }
    var setpreviewInvoiceFullDetails = function (previewInvoiceFullDetails) {

        invoicePreviewconfig = previewInvoiceFullDetails;
    }
    var getpreviewInvoiceFullDetails = function () {
        return invoicePreviewconfig;
    }
 var setInvoiceConfig=function(invoicefull)
 {
 invoicefullconfig=invoicefull;
 }
 var getInvoiceConfig=function()
 {
 return invoicefullconfig
 }
var setdate1=function(date1)
{
invoicedate=date1;
console.log(invoicedate);
}
var getdate1=function()
{
console.log(invoicedate);
return invoicedate;
}

var setdate2=function(date2)
{
duedate=date2;
console.log(duedate);
}
var getdate2=function()
{
console.log(duedate);
return duedate;
}
    return {
        getInvoiceJsonConfig: getInvoiceJsonConfig,
        setInvoiceFromConfig: setInvoiceFromConfig,
        getInvoiceFromConfig: getInvoiceFromConfig,
        saveInvoiceDetails: saveInvoiceDetails,
        getInvoiceDetailsByRange: getInvoiceDetailsByRange,
        getInvoiceDetailsCount: getInvoiceDetailsCount,
        getInvoiceCount: getInvoiceCount,
        setInvoiceCount: setInvoiceCount,
        updateInvoiceDetails: updateInvoiceDetails,
        deleteInvoiceDetails: deleteInvoiceDetails,
        getInvoiceDetailsByName: getInvoiceDetailsByName,
        getInvoiceDetailsById: getInvoiceDetailsById,
        getAllInvoiceName: getAllInvoiceName,
        getAllInvoice: getAllInvoice,
        setpreviewInvoiceFullDetails:setpreviewInvoiceFullDetails,
        getpreviewInvoiceFullDetails:getpreviewInvoiceFullDetails,
        setInvoiceConfig:setInvoiceConfig,
        getInvoiceConfig:getInvoiceConfig,
        setdate1:setdate1,
        getdate1:getdate1,
        setdate2:setdate2,
        getdate2:getdate2

         }
})